# Fakelog

Some times it is nice to see something happen.

## Installation

```bash
$ sudo npm install -g fakelog
```

## Usage

```bash
$ fakelog
```

#### License: MIT
#### Author: [Ash Brown](https://bitbucket.org/bedican)
